import { Component } from '@angular/core';
import { CardService } from '../services/card.service';
import { Department } from '../interfaces/department.interface';
import { Sucursal } from '../interfaces/sucursal.interface';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.css'],
})
export class CardComponent {
  constructor(private cardservice: CardService) {
    this.cardservice.getData().subscribe((resp) => {
      this.sucursal = resp;
      console.log(this.sucursal);
    });
  }
  sucursal: any;
}
