export interface Sucursal {
  address: string;
  department: string;
  id: number;
  location: string;
  municipality: string;
}
