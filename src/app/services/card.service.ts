import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Sucursal } from '../interfaces/sucursal.interface';
import { Department } from '../interfaces/department.interface';

@Injectable({
  providedIn: 'root',
})
export class CardService {
  constructor(private http: HttpClient) {}
  //todos los datos
  getData(): Observable<Sucursal> {
    return this.http.get<Sucursal>('http://localhost:3000');
  }

  //datos de los departamentos
  getDepartments(): Observable<Department> {
    return this.http.get<Department>('http://localhost:3000/departments');
  }
}
