import { Component, OnInit } from '@angular/core';
import { CardService } from '../services/card.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css'],
})
export class HeaderComponent {
  constructor(private cardservice: CardService) {
    this.cardservice.getDepartments().subscribe((resp) => {
      this.departments = resp;
    });
  }
  departments: any;
}
